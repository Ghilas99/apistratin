//importation
const jwt=require("jsonwebtoken");
const dotenv= require("dotenv");

//exportation de la fonction
module.exports=(req,res,next)=>{
try{
    //recupérer le token
    const token= req.headers.authorization.split(" ")[1];

    //verfier le token 
    if(token){
        next();
    }else{
        throw " ";
    }

}catch(error){
    res.status(500).json({error:"Token is empty"});
}

};