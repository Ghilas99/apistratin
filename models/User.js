'use strict';

//importation de mongoose
const mongoose= require("../db/db");


//model de base pour  creer un compte

const userSchema=mongoose.Schema({
    email:{type: String,required:true, unique: true},
    password:{type: String,required:true}
});


module.exports = mongoose.model("user",userSchema);