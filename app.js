//mon api
//pour gerer les requettes

const express = require("express");
//importation de body-parser
const bodyParser= require("body-parser");

//importation de mongoose
const mongoose= require("./db/db");

//importation des routes
const userRoutes= require("./routes/user");

const app= express();

//transformer le body en json(bodyparser)
app.use(bodyParser.json());


//les routes generales
app.use(userRoutes);

//pour l'exportation
module.exports=app;