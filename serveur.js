//importer http de nodejs
const http = require ("http");

//importer le package pour utuliser les variables d'enverement
const dotenv=require("dotenv");
const result = dotenv.config();

//importer l'application app.js
const app= require("./app");
app.set("port",process.env.PORT);

//creation de serveur
const serveur=http.createServer(app);
serveur.listen(process.env.PORT);