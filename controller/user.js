//importation models de la bdd
 const User= require("../models/User");
 //importation de jsonwebtoken
 const jwt= require("jsonwebtoken");

 //importation des valeurs d'enveronement
 const dotenv=require("dotenv");
 // Constants pour controler le mot de passe et l'email avat de l'envoyer a la bdd
const EMAIL_REGEX     = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX  = /^(?=.*\d).{4,8}$/;

 //la fonction signup pour enregistrer un nouveau  user dans la bdd
 exports.signup=(req , res, next)=>{
    
    if (!EMAIL_REGEX.test(req.body.email)) {
        return res.status(500).json({ error: 'email is not valid' });
    };
  
      if (!PASSWORD_REGEX.test(req.body.password)) {
        return res.status(500).json({ error: 'password invalid (must length 4 - 8 and include 1 number at least)' });
    };
    const user = new User({
        email :req.body.email ,
        password :req.body.password
    });
    console.log(user);
    user.save()
    .then(()=>res.status(201).json({message : "User is created"}))
    .catch(()=>res.status(400).json({error: "email already exists\,create failure" }));
    
}
  //la fonction login
  exports.login=(req , res, next)=>{
    
    if (!EMAIL_REGEX.test(req.body.email)) {
        return res.status(400).json({ error: "email is not valid" });
    };
  
      if (!PASSWORD_REGEX.test(req.body.password)) {
        return res.status(400).json({ error: "password invalid (must length 4 - 8 and include 1 number at least)" });
    };
    User.findOne({email: req.body.email})
    .then((user)=>{
        
        if(!user.password == req.body.password){
            return res.status(400).json({ error: "incorrect password" });
        }
        //password est correct
        //
        res.status(201).json({
            userId: user._id,
            token:jwt.sign({userId: user._id},`${process.env.JWT_KEY_TOKEN}`,{expiresIn: "1h"})
        });
    })
    .catch(()=>res.status(500).json({error: "user not found "}));
 };

  //la fonction signup pour enregistrer un nouveau  user dans la bdd
  exports.getUsers=(req , res)=>{
    User
    .find()
    .then((allUsers)=>res.status(200).json({allUsers}))
    .catch(()=>res.status(400).json({error: "user not found "}));
 };
