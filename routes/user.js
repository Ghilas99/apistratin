//importation express
const express = require("express");
//imporation du controller/user
const userController= require("../controller/user");

//importation du middleware d'autentification 
const authentification=require("../middleware/authentification");


//la fonction Route
const router = express.Router()


router.post("/register" , userController.signup);

router.post("/login" ,userController.login );

router.get("/user" , authentification, userController.getUsers );

//exportation du module
module.exports  = router;
